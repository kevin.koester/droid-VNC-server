# droid-vnc-server

VNC server for Android, tested working with Jelly Bean till Marshmallow

## Getting started

Make sure Android SDK and NDK are installed

    bash build.sh -wa API_LEVEL

In some cases setting the environment variable APP_ALLOW_MISSING_DEPS=true is necessary.
 
Specify API level with `-a`

Use `-w` flag to build the wrapper libs; AOSP source is required at `../aosp`

Wrapper libs for API 19 to 25 have been prebuilt in [](nativeMethods/libs/armeabi-v7a/) and should work out of the box

Using it over adb:

adb forward tcp:5901 tcp:5901
adb forward tcp:5801 tcp:5801

connect to localhost:5901
